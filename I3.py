import requests

host = "data.usajobs.gov"
userAgent = "jwgarcia@uw.edu"
authKey = "YourAPIkey"

url = 'https://data.usajobs.gov/api/search'
headers = {
    'Host': host,
    'User-Agent': userAgent,
    'Authorization-Key': authKey
}

response = requests.get(url, headers=headers)

data = response.json()